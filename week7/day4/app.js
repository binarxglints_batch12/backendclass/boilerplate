// Require dotenv
require('dotenv').config();

const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

// Import router
const userRoute = require('./routes/userRoute');

const port = process.env.PORT || 5000;
const app = express();

// To handle req.body (json and url-encoded)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// To handle req.body (form-data)
app.use(fileUpload());

// Using cors
app.use(cors());

// Using public folder to save the image and access the image
app.use(express.static('public'));

// Define routes
app.use('/users', userRoute);

// Upload image
app.post('/upload', (req, res) => {
  if (!req.files) {
    return res.status(400).json({ message: 'File is required' });
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  const file = req.files.file;
  const uploadPath = __dirname + '/public/images/' + file.name;

  // Use the mv() method to place the file somewhere on your server
  file.mv(uploadPath, function (err) {
    if (err) return res.status(500).send({ message: err.message });

    return res.status(201).send({
      message: 'File uploaded',
      url: `http://localhost:${port}/images/${file.name}`,
    });
  });
});

app.all('*', (req, res) => {
  return res.status(404).json({ message: 'Not Found' });
});

app.listen(port, () => console.log(`Listening on port ${port}`));
