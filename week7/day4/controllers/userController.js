const { user } = require('../models');

exports.create = async (req, res, next) => {
  try {
    if (!req.files) {
      return res.status(400).json({ message: 'File is required' });
    }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    const profilePicture = req.files.profilePicture;
    const uploadPath = __dirname + '/../public/images/' + profilePicture.name;

    // Use the mv() method to place the file somewhere on your server
    profilePicture.mv(uploadPath, async function (err) {
      if (err) return res.status(500).send({ message: err.message });

      const createdUser = await user.create({
        name: req.body.name,
        profilePicture: `${profilePicture.name}`,
      });

      return res.status(201).send({ data: createdUser });
    });
  } catch (error) {
    return res.status(500).send({ message: error.message });
  }
};
