const mongoose = require('mongoose'); // Import mongoose

// Make user schema
const UserSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    profilePicture: {
      type: String,
      required: true,
      get: getProfilePicture,
    },
  },
  {
    // Enable timestamps, it will auto create createdAt and updatedAt column to know when data has been created and updated
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
    toJSON: { getters: true }, // Enable getter
  }
);

function getProfilePicture(picture) {
  return `/images/${picture}`;
}

module.exports = mongoose.model('user', UserSchema); // Export user models
