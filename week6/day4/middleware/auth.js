const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { user } = require('../models'); // Import user model

exports.register = async (req, res, next) => {
  try {
    // Make user
    const createdUser = await user.create(req.body);

    req.user = createdUser;

    next();
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};

exports.login = async (req, res, next) => {
  try {
    // Find user is exist or
    const userLogin = await user.findOne({
      email: req.body.email,
    });

    // If userLogin doesn't exist
    if (!userLogin) {
      return res.status(401).json({
        message: 'Email is not found',
      });
    }

    // verify the password
    const verifyPassword = await bcrypt.compare(
      req.body.password,
      userLogin.password
    );

    // If password doesn't match
    if (!verifyPassword) {
      return res.status(401).json({
        message: 'Wrong Password',
      });
    }

    req.user = userLogin;

    next();
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};

exports.user = async (req, res, next) => {
  try {
    const decoded = jwt.verify(
      req.headers.authorization.replace('Bearer ', ''),
      process.env.TOKEN_SECRET
    );

    const data = await user.findOne({ _id: decoded.user.id });

    if (data.role !== 'user') {
      return res.status(403).json({
        message: 'Forbidden',
      });
    }

    req.user = decoded.user;

    next();
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};

exports.admin = async (req, res, next) => {
  try {
    const decoded = jwt.verify(
      req.headers.authorization.replace('Bearer ', ''),
      process.env.TOKEN_SECRET
    );

    const data = await user.findOne({ _id: decoded.user.id });

    if (data.role !== 'admin') {
      return res.status(403).json({
        message: 'Forbidden',
      });
    }

    req.user = decoded.user;

    next();
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};

exports.adminOrUser = async (req, res, next) => {
  try {
    const decoded = jwt.verify(
      req.headers.authorization.replace('Bearer ', ''),
      process.env.TOKEN_SECRET
    );

    const data = await user.findOne({ _id: decoded.user.id });

    if (data.role === 'admin' || data.role === 'user') {
      req.user = decoded.user;

      return next();
    }

    return res.status(403).json({
      message: 'Forbidden',
    });
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};
