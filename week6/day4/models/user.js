const mongoose = require('mongoose'); // Import mongoose
const bcrypt = require('bcrypt'); // Import bcrypt for password encryption

// Make user schema
const UserSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      set: encryptPassword, // It will call the encryptPassword function before save to database
    },
    role: {
      type: String,
      required: true,
      default: 'user',
    },
  },
  {
    // Enable timestamps, it will auto create createdAt and updatedAt column to know when data has been created and updated
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
  }
);

// This will be running when user create or change the password
function encryptPassword(password) {
  const encryptedPassword = bcrypt.hashSync(password, 10);
  return encryptedPassword;
}

module.exports = mongoose.model('user', UserSchema); // Export user models
