// Require dotenv
require("dotenv").config();

const express = require("express");
const cors = require("cors");

// Import router
const authRoutes = require("./routes/authRoutes");

const port = process.env.PORT || 5000;
const app = express();

// To read req.body
app.use(express.json()); // Enable req.body JSON type (raw)
app.use(
  express.urlencoded({
    extended: false,
  })
); // Support urlencode body
app.use(cors());

// Routing
app.use("/auth", authRoutes);

// 404 Not Found
app.all("*", (req, res) =>
  res.send("You've tried reaching a route that doesn't exist.")
);

app.listen(port, () => console.log(`Server running on port ${port}`));
