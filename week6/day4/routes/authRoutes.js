const express = require('express');
const router = express.Router();

// Import auth middleware
const {
  register,
  login,
  user,
  admin,
  adminOrUser,
} = require('../middleware/auth');

// Import controller
const {
  getToken,
  getProfile,
  getUser,
} = require('../controllers/authController');

router.post('/register', register, getToken);
router.post('/login', login, getToken);
router.get('/me', adminOrUser, getProfile);
router.get('/:id', admin, getUser);

module.exports = router;
