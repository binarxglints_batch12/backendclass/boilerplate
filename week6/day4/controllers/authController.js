const jwt = require('jsonwebtoken');
const { user } = require('../models'); // Import user model

// Make a register function and export it
exports.getToken = async (req, res, next) => {
  try {
    // Create token for that user
    const token = jwt.sign(
      { user: { id: req.user._id } },
      process.env.TOKEN_SECRET,
      { expiresIn: '1d' }
    );

    // Return token to frontend, so they can save that token
    return res.status(200).json({ token });
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};

exports.getProfile = async (req, res, next) => {
  try {
    // Find the user
    const data = await user.findOne({ _id: req.user.id });

    return res.status(200).json({ data });
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};

exports.getUser = async (req, res, next) => {
  try {
    // Find the user
    const data = await user.findOne({ _id: req.params.id });

    return res.status(200).json({ data });
  } catch (error) {
    // If error it will make status code 500 (Internal Server Error) and send the error message
    return res.status(500).json({
      message: error.message,
    });
  }
};
